<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Cliente;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
	    $em = $this->getDoctrine()->getManager();

	try{
	    $cliente = new Cliente();
	    $cliente->setNombre("Nicolas");
	    $cliente->setApellido("Bullorin");
	    $cliente->setNacimiento(new \DateTime());
	    $em->persist($cliente);
	    $em->flush();
	}catch(\Exception $e){
	    die("X");
	}
        return $this->render('AppBundle::base.html.twig');
    }

    /**
     * @Route("/check", name="check") 
     */
    public function checkAction(Request $request)
    {
	$em = $this->getDoctrine()->getManager();
	$cliente = $em->getRepository("AppBundle:Cliente")->find(1);
	return $this->render('AppBundle::base.html.twig', array("cliente" => $cliente));
    } 

    /**
     * @Route("/mostrar/{texto}", name="mostrar-texto")
     */
    public function mostrarTextoAction(Request $request, $texto)
    {
	die("X: " . $texto);
    }

    /**
     * @Route("/json/{idCliente}", name="json-cliente")
     */
    public function jsonClienteAction(Request $request, $idCliente)
    {
	$em = $this->getDoctrine()->getManager();
	$cliente = $em->getRepository("AppBundle:Cliente")->find($idCliente);
	return new JsonResponse(array("data" => $cliente));
    }

}
